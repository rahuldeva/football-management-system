import { ShowPointtableComponent } from './show-pointtable/show-pointtable.component';
import { ShowTeamsComponent } from './show-teams/show-teams.component';
import { ShowScoreComponent } from './show-score/show-score.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShowScheduleComponent } from './show-schedule/show-schedule.component';
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  
  { path: 'home', component: HomeComponent,
  },

  { 
    path: 'home',
    component: HomeComponent,
    children: [
      { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
      { path: 'manager', loadChildren: () => import('./manager/manager.module').then(m => m.ManagerModule) },
      { path: 'player', loadChildren: () => import('./player/player.module').then(m => m.PlayerModule) },
      { path: 'coach', loadChildren: () => import('./coach/coach.module').then(m => m.CoachModule) },
      { path: 'player', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) }
    ],
  },
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  {path: 'show-shedule', component: ShowScheduleComponent},
  {path: 'show-score', component:ShowScoreComponent},
  {path:'show-teams',component:ShowTeamsComponent},
  {path:'show-pointtable',component:ShowPointtableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
