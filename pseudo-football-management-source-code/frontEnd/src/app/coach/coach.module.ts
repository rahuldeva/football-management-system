import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoachRoutingModule } from './coach-routing.module';
import { CoachDashComponent } from './coach-dash/coach-dash.component';


@NgModule({
  declarations: [CoachDashComponent],
  imports: [
    CommonModule,
    CoachRoutingModule
  ]
})
export class CoachModule { }
