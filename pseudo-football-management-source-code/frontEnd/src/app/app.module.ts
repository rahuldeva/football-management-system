import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ShowScheduleComponent } from './show-schedule/show-schedule.component';
import { ShowScoreComponent } from './show-score/show-score.component';
import { ShowTeamsComponent } from './show-teams/show-teams.component';
import { ShowPointtableComponent } from './show-pointtable/show-pointtable.component';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [ //no of component here
    AppComponent,
    HomeComponent,
    ShowScheduleComponent,
    ShowScoreComponent,
    ShowTeamsComponent,
    ShowPointtableComponent,
  ],
  imports: [ //required modules
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
