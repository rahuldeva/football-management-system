import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontEnd';
  constructor(private router:Router){}
  onLogIn(){
  this.router.navigate(['/auth/signin'])
  }
}
