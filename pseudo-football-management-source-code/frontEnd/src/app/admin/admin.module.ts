import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { SheduleMatchComponent } from './shedule-match/shedule-match.component';


@NgModule({
  declarations: [AdminHomeComponent, SheduleMatchComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
