import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  email=""
  password=""
  url="http://localhost:8080/user"
  constructor(private http:HttpClient,
    private router:Router) { }
  signin(email,password)
  {
    const body={
      email:email,
      password:password
    }
    return this.http.post(this.url+'/login',body)
  }

}
