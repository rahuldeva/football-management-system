import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  email=""
  password=""
  constructor(private toastr:ToastrService,
              private authService:AuthService,
              private router:Router) { }

  ngOnInit(): void {}
  signIn(){
    console.log(this.email);
    if(this.email.length==0)
    {
      this.toastr.warning('please enter email')
     // alert("Please Enter Email")
    }
    else if(this.password.length==0)
    {
      this.toastr.warning("Please Enter Password")
      //alert("Please Enter password")
    }else{
      
    this.authService.signin(this.email,this.password).subscribe(response=>{
      console.log(response)
      if(response['status']==500)
      {
          console.log("done")
      //   this.toastr.success(`Welcome..  ${response['data'].firstName}   ${response['data'].lastName}`);
      //  sessionStorage['user_name']=response['data'].firstName +' '+response['data'].lastName
      //   sessionStorage['token']=response['data'].token
      //   this.router.navigate(['/home/product/gallery'])
      }
      else
      {
        this.toastr.error("Invalid credentials")
       
      }
    })
  }
  }
  signup(){}


}
